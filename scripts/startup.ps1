﻿$File = "C:\startup\startup"

Write-Host "initialising system monitor..."
"WAITING" | out-file $File

Write-Host "Settings up production environment..."
$env:NODE_ENV="production"

Start-Sleep -s 3

$Action = { 
    Write-Host "the file was changed"
    Write-Host "computer restart"
    # stop all current activities just in case
    Invoke-Expression -command "forever stopall"
    Restart-Computer localhost
 }
$global:FileChanged = $false

Write-Host "setting up system watcher..."
function Wait-FileChange {
    param(
        [string]$File,
        [string]$Action
    )
    $FilePath = Split-Path $File -Parent
    $FileName = Split-Path $File -Leaf
    $ScriptBlock = [scriptblock]::Create($Action)

    $Watcher = New-Object IO.FileSystemWatcher $FilePath, $FileName -Property @{ 
        IncludeSubdirectories = $false
        EnableRaisingEvents = $true
    }
    $onChange = Register-ObjectEvent $Watcher Changed -Action {$global:FileChanged = $true}

    while ($global:FileChanged -eq $false){
        Start-Sleep -Milliseconds 100
    }

    & $ScriptBlock 
    Unregister-Event -SubscriptionId $onChange.Id
}

#startup preparations
Write-Host "cleaning up working directory..."
Invoke-Expression -command "git reset --hard origin/master"
Invoke-Expression -command "git clean -f -d"

Write-Host "cleaning up chrome instances..."
Invoke-Expression -command "Taskkill /IM chrome.exe"
Start-Sleep -s 3

Write-Host "cleaning up node instances..."
Invoke-Expression -command "taskkill /F /IM node.exe"
Invoke-Expression -command "forever stopall"
Start-Sleep -s 3

Write-Host "starting kiosk-proxy..."
Invoke-Expression -command "cd c:\startup\box\"
Invoke-Expression -command "forever start -t -w -a --watchIgnore '*.log' -l proxy.log -o out.log -e  err.log .\proxy.js" | Out-Host


Wait-FileChange -File $File -Action $Action
