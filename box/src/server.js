var express = require('express'),
    compression = require('compression'),
    logger = require('../tools/logger'),
    jsx = require('node-jsx').install(),
    path = require('path'),
    configureRoutes = require('./routes'),
    config = require('./config');

var server = express();
var port = config.port;
var ONE_DAY = 86400000;

server.set('view engine', 'handlebars');
server.set('views', __dirname + '/views');
server.use(compression());

server.use(express.static(path.join(__dirname, '/public'), { maxAge: ONE_DAY }));
configureRoutes(server);

server.listen(port, function(){
    logger.info('Express server listening on port ' + port);
});

process.on('SIGINT', function(){
    logger.info("shutting down the server.js");
    process.exit(0);
});
