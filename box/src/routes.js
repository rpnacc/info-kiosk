var React = require('react'),
    logger = require('../tools/logger'),
    ReactDOM = require('react-dom/server'),
    NotFoundPage = React.createFactory(require('./components/NotFoundPage')),
    ErrorPage = React.createFactory(require('./components/ErrorPage')),
    //KioskApp = React.createFactory(require('./components/KioskApp')),
    Html = React.createFactory(require('./components/Html'));

module.exports = function(app){

    require('./api/content')(app);

    app.get('/', function(req, res, next){
        try {
            var categories = req.categories || [],
                contacts = req.contacts || [];

            var data = {
                categories : categories,
                contacts: contacts
            };

            //var body = ReactDOM.renderToString(KioskApp(data));

            var initial = JSON.stringify(data);

            var html = ReactDOM.renderToStaticMarkup(
                Html({
                    initial: initial
                })
            );

            res.send('<!doctype html>\n' + html);
        } catch (err) {
            next(err);
        }
    });

    /**
     * Error handling
     */
    app.use(function(err, req, res, next){
        logger.error(err.Message);
        logger.error(err.stack);

        // treat as 404
        if (err.message
            && (~err.message.indexOf('not found')
            || (~err.message.indexOf('Cast to ObjectId failed')))) {
            return next();
        }

        // error page
        res.status(err.status || 500);

        var html = ReactDOM.renderToStaticMarkup(
            ErrorPage({error: err.message})
        );

        res.send(html);
    });

    // assume 404 since no middleware responded
    app.use(function (req, res) {
        res.status(404);

        var html = ReactDOM.renderToStaticMarkup(
            NotFoundPage({})
        );

        res.send(html);
    });
};