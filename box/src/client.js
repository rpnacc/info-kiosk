/** @jsx React.DOM */

var React = require('react'),
    ReactDOM = require('react-dom'),
    KioskApp = require('./components/KioskApp');


var rawState = document.getElementById('initial-data').innerHTML;
var state = rawState !== "" ? JSON.parse(rawState) : [];
window.isOpenModal = false;

ReactDOM.render(
    <KioskApp categories={ state.categories } contacts={ state.contacts }/>,
    document.getElementById('react-app')
);