var fs = require('fs'),
    search = require('./search'),
    logger = require('../../tools/logger'),
    path = require('path'),
    config = require('../config');


var ASSETS_PATH = path.join(config.root, "/public"),
    CONTENT_PATH = path.join(ASSETS_PATH, config.contentDirectory);

var formatContent  = function(files, container){
    if(files && files instanceof Array){
        files.forEach(function(file) {
            container.push(file.replace(ASSETS_PATH, ""));
        });
    }
};

var findAndFormatContent = function(dir, ext, container){
    var items = search(dir, ext);
    formatContent(items, container);
};

module.exports = function(app){
  var cache = {
    categories: undefined,
    contacts: undefined
  };

  app.get('/', function(req, res, next) {
    try {

      if(cache.categories){
        req.categories = cache.categories;
        req.contacts = cache.contacts;
        return next();
      }

      fs.readdir(CONTENT_PATH, function(err, directories){
          if(err){
              return next(err);
          }

          var categories = [];

          directories.forEach(function(dir){
              try {
                  var videosDir = path.join(CONTENT_PATH, dir, config.contentVideosDirectoryName),
                      filmDir = path.join(CONTENT_PATH, dir, config.contentFilmDirectoryName),
                      documentsDir = path.join(CONTENT_PATH, dir, config.contentDocumentsDirectoryName),
                      contentPath = path.join(CONTENT_PATH, dir, config.contentFileName);

                  if(!fs.existsSync(contentPath)){
                      return;
                  }

                  var category = {
                      Documents: [],
                      Videos: [],
                      FilmVideos: [],
                      Content: {}
                  };

                  var data = fs.readFileSync(contentPath, 'utf8');
                  category.Content = JSON.parse(data);

                  if(category.Content.HasExtras){
                      findAndFormatContent(filmDir, ".mp4", category.FilmVideos);
                  }
                  findAndFormatContent(videosDir, ".mp4", category.Videos);
                  findAndFormatContent(documentsDir, ".pdf", category.Documents);

                  categories.push(category);
              }
              catch(err){
                  logger.error("An error occurred during categories.json parsing. " + err.Message);
              }
          });

          var contactsPath = path.join(CONTENT_PATH, config.contactsFileName);
          var data = fs.readFileSync(contactsPath, 'utf8');
          req.contacts = JSON.parse(data);
          req.categories = categories;

          cache.categories = categories;
          cache.contacts = req.contacts;
          next();
      });


    }
    catch (err) {
      next(err);
    }
  });
};