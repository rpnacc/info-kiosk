/** @jsx React.DOM */

var React = require('react');

var title = "Страница не найдена.";

module.exports = NotFoundPage = React.createClass({
  render: function() {

    return (
      <div>
        <h1>{ title }</h1>
        <p>Извините, но страница которую вы запросили не была найдена.</p>
      </div>
    );
  }
});