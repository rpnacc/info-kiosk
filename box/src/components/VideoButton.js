/** @jsx React.DOM */

var React = require('react'),
    Modal = require('./Modal'),
    ModalOpenCloseMixin = require('./Mixes/ModalOpenCloseMixin'),
    VideoPlayer = require('./Video/VideoPlayer');

var title = "СМОТРЕТЬ ВИДЕО";

module.exports = VideoButton = React.createClass({
    propTypes: {
        videos: React.PropTypes.array.isRequired,
        btnTitle: React.PropTypes.string,
        displayTimeoutMs: React.PropTypes.number,
        idleLimitInMs: React.PropTypes.number
    },
    mixins: [ModalOpenCloseMixin],

    getDefaultProps: function(){
        return {
            btnTitle: title,
            videos: [],
            displayTimeoutMs: 5000, // 5s
            idleLimitInMs: 20000 // 20 s
        }
    },

    getInitialState: function() {
        return {
            modalIsOpen: false,
            currentVideoIndex: 0,
            isTrackingEnabled: false
        };
    },

    onPauseVideo: function(){
        if(!this.isMounted()) {
            return;
        }

        this.state.setState({
            isTrackingEnabled: true
        });
    },

    onPlayVideo: function(){
        if(!this.isMounted()) {
            return;
        }
        this.state.setState({
            isTrackingEnabled: false
        });
    },

    onNextVideo: function() {
        if(!this.isMounted()) {
            return;
        }
        var self = this;
        var index = this.state.currentVideoIndex;

        if(index + 1 < this.props.videos.length) {
            setTimeout(function() {
                self.setState({ currentVideoIndex: index + 1 });
            }, this.state.displayTimeoutMs);
        }
        else{ // close the player 5 s after last video
            setTimeout(function() {
                self.closeModal();
            }, this.state.displayTimeoutMs);
        }
    },


    render: function() {
        var currentVideo = this.props.videos[this.state.currentVideoIndex];
        var self = this;

        var videoPLayer;
        if(currentVideo){
            videoPLayer =  <VideoPlayer id="videoViewer"
                src={ currentVideo }
                resize={ true }
                onNextVideo={ self.onNextVideo }
                onNextVideo={ self.onPauseVideo }
                onNextVideo={ self.onPlayVideo }
                endlessMode={ true }
            />
        }

        return (
            <div className="i-btns__item wow fadeIn animated">
                <button className="btn i-btns__btn" onClick={ this.openModal }>
                    <i className="ico ico-arrow i-btns__icon"/>
                    <span className="i-btns__text">{ this.props.btnTitle }</span>
                </button>

                <Modal isModalOpen={ self.state.modalIsOpen }
                       isTrackingEnabled = { self.state.isTrackingEnabled }
                       contentId = { "videoViewer" }
                       activityTimeoutInMs = { self.props.idleLimitInMs }
                       closeModal =  { self.closeModal }>
                    { videoPLayer }
                </Modal>
            </div>
        )
    }
});