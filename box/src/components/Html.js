/** @jsx React.DOM */

var React = require('react'),
    ReactDOM = require('react-dom');

var title = "Инфокиоск";

module.exports = Html = React.createClass({
    propTypes: {
        body: React.PropTypes.string
    },
    render: function() {

        return (
            <html lang="en">

                <head>
                    <title>{ title }</title>

                    <meta charSet="utf-8"/>
                    <meta name="robots" content="index, follow" />
                    <meta name="viewport" content="width=device-width, initial-scale=0.75, maximum-scale=1.0, user-scalable=no" />

                    <link rel="stylesheet" type="text/css" href="css/animate.css"/>
                    <link rel="stylesheet" type="text/css" href="css/custom.css"/>
                    <link rel="stylesheet" type="text/css" href="css/global-import.css"/>
                    <link rel="stylesheet" type="text/css" href="css/video-js.css"/>

                    <script src="js/swiper.min.js"></script>
                </head>

                <body>
                    <div id="react-app"></div>

                    <div id="startup-data">
                        <script type="application/json" id="initial-data" dangerouslySetInnerHTML={{ __html: this.props.initial }}/>
                    </div>

                    <footer>
                        <script src="js/bundle.js"></script>
                        <script src="js/video.js"></script>
                        <script src="js/pdfjs-dist/build/pdf.js"></script>
                    </footer>

                </body>

            </html>
        )
    }
});