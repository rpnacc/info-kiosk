/** @jsx React.DOM */

var React = require('react'),
    ReactDOM = require('react-dom'),
    SetIntervalMixin = require('./Mixes/SetIntervalMixin'),
    ReactModal = require('react-modal');

var customStyles = {
    content : {
        overflow              : 'visible',
        padding               : 'none'
    }
};
var defaultOutageLimitInMs = 30000;
var nope = function() {};

module.exports = Modal = React.createClass({
    propTypes: {
        isModalOpen: React.PropTypes.bool.isRequired,
        isTrackingEnabled: React.PropTypes.bool,
        activityTimeoutInMs: React.PropTypes.number,
        closeModal: React.PropTypes.func.isRequired,
        contentId: React.PropTypes.string
    },
    mixins: [SetIntervalMixin],

    getDefaultProps: function() {
        return {
            isModalOpen: false,
            isTrackingEnabled: true,
            activityTimeoutInMs: defaultOutageLimitInMs,
            closeModal: nope,
            contentId: undefined
        }
    },

    componentDidUpdate: function(){
        var self = this;
        setTimeout(function(){
            if(!self.props.isTrackingEnabled){
                return;
            }

            var view = self.getTarget(self.props.contentId);

            if(view){
                view.addEventListener('mouseover', self.mouseOverModal);
                view.addEventListener('touchstart', self.handleEventsFromChild);
                view.addEventListener('click', self.handleEventsFromChild);
                window.addEventListener("message", self.handleEventsFromChild, false);
            }
        }, 2000);
    },
    componentWillUnmount: function() {
        if(!this.props.isTrackingEnabled){
            return;
        }

        var self = this;

        var view = self.getTarget(self.props.contentId);

        if(view){
            view.removeEventListener('mouseover', self.mouseOverModal);
            view.removeEventListener('touchstart', self.handleEventsFromChild);
            view.removeEventListener('click', self.handleEventsFromChild);
            window.removeEventListener("message", self.handleEventsFromChild, false);
        }
    },

    mouseOverModal: function(e){
        this.seconds = 0;
    },
    handleEventsFromChild: function(e){
        this.seconds = 0;
    },

    getTarget: function(contentId) {
        if(!contentId){
            return null;
        }
        var node = document.getElementById(contentId);

        if(node){
            return node.parentElement;
        }
        return node;
    },

    tick: function(){
        if(this.seconds >= this.props.activityTimeoutInMs){
            this.clearIntervals();
            this.props.closeModal();
        }

        if(this.props.isModalOpen){
            this.seconds += 1000;
        }
    },

    startTracking: function(){
        if(this.props.isModalOpen && this.props.isTrackingEnabled){
            this.setInterval(this.tick, 1000);
        }
    },

    render: function() {
        this.seconds = 0;
        this.startTracking();

        return (
                <ReactModal ref="target"
                       isOpen={ this.props.isModalOpen }
                       onRequestClose={ this.props.closeModal }
                       style= {customStyles}>
                            { this.props.children }
                            <button type="button" className="btn i-popup__close js-popup-btn" onClick={ this.props.closeModal }>&times;</button>
                </ReactModal>
        );
    }
});
