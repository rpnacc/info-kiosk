/** @jsx React.DOM */

var React = require('react'),
    DocumentsButton = require('./DocumentButton'),
    VideoButton = require('./VideoButton');

module.exports = CategoryButtons = React.createClass({
    propTypes: {
        category: React.PropTypes.object.isRequired
    },

    render: function() {
        var category = this.props.category;

        var videos = category.Videos;
        var documents = category.Documents;
        var filmVideos = category.FilmVideos;
        var content = category.Content;

        var documentButton, videoButton, filmButton;
        if(documents.length > 0){
            documentButton = <DocumentsButton key={content.Key + "DocumentsButton"} documents={ documents }/>
        }

        if(videos.length > 0){
            videoButton = <VideoButton key={content.Key + "VideoButton"} videos={ videos }/>
        }

        if(filmVideos.length > 0 && content.HasExtras){
            filmButton = <VideoButton key={content.Key + "FilmButton"} videos={ filmVideos } btnTitle="СМОТРЕТЬ ФИЛЬМ"/>
        }

        var buttonsBlockClass = content.HasExtras ? "i-btns i-btns_big" : "i-btns";

        return (
            <div className={ buttonsBlockClass }>
                { documentButton }
                { videoButton }
                { filmButton }
            </div>
        )
    }
});