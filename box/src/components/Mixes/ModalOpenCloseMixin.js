module.exports = ModalOpenCloseMixin = {
    openModal: function() {
        this.setState({ modalIsOpen: true });
        window.isOpenModal = true;
    },

    closeModal: function() {
        this.setState({ modalIsOpen: false });
        window.isOpenModal = false;
    }
};