/** @jsx React.DOM */

var React = require('react');

var title = "Ошибка.";

module.exports = NotFoundPage = React.createClass({
  render: function() {
      return (
        <div>
          <h1>{title}</h1>
          <p>Извините, возникла критическая ошибка - { this.props.error }</p>
        </div>
      );
  }
});
