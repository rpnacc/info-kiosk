/** @jsx React.DOM */

var React = require('react'),
    ModalOpenCloseMixin = require('./Mixes/ModalOpenCloseMixin'),
    Modal = require('./Modal');

module.exports = Contacts = React.createClass({
    propTypes: {
        contacts: React.PropTypes.array.isRequired,
    },
    mixins: [ModalOpenCloseMixin],

    getInitialState(){
        return {
            contactUrl: undefined,
            modalIsOpen: false
        }
    },

    getDefaultProps: function(){
        return {
            idleLimitInMs: 90000 // 90 s
        }
    },

    openWebView: function(event){
        var node = event.target;

        var element = node.querySelector(".i-contacts__link") == undefined
            ? node.parentNode.querySelector(".i-contacts__link")
            : node.querySelector(".i-contacts__link");

        this.setState({
            contactUrl:  element.getAttribute("data-href")
        });

        this.openModal();
    },

    render: function() {

        var self = this;

        var elements = this.props.contacts.map(function(contact){
            if (contact.Url !== "" ){
                return (
                    <div key = { contact.Name } className="i-contacts__row" onClick={ self.openWebView }>
                        <span>{ contact.Name }</span>
                        <br />
                        <a className="i-contacts__link" data-href={ contact.Url }>{ contact.DisplayName }</a>
                    </div>
                );
            }
            return (
                <div key = { contact.Name } className="i-contacts__row">
                    <span>{ contact.Name }</span>
                    <br />
                    <a className="i-contacts__text"> { contact.DisplayName } </a>
                </div>
            );
        });

        var view;

        if(this.state.contactUrl){
            view = <iframe src= { this.state.contactUrl } className="frame-viewer" id="contacts-viewer" sandbox="allow-scripts"></iframe>
        }

        return (
            <div className="i-contacts wow fadeIn animated">
                <div className="i-contacts__heading">
                    Контакты
                    <br />
                    для обращений
                </div>
                { elements }

                <Modal isModalOpen={ self.state.modalIsOpen }
                       contentId = { "contacts-viewer" }
                       activityTimeoutInMs = { this.props.idleLimitInMs }
                       isTrackingEnabled = { true }
                       closeModal =  { self.closeModal }>
                    { view }
                </Modal>
            </div>
        )
    }
});