/** @jsx React.DOM */

var React = require('react');

module.exports = Category = React.createClass({
    propTypes: {
        name: React.PropTypes.any,
        showCategoryTitle: React.PropTypes.bool,
        iconClass: React.PropTypes.string.isRequired,
        isSelected: React.PropTypes.bool.isRequired,
    },

    render: function() {

        var categoryClass = this.props.isSelected  ?
            "btn slider__item slider_nav__item swiper-slide-active swiper-slide" :
            "btn slider__item slider_nav__item swiper-slide";

        var iconClass = "slider_nav__icon ico " + this.props.iconClass;
        var name = this.props.name;

        var heading;
        if(this.props.showCategoryTitle) {
            if(name instanceof Array){
                var children = [];
                name.forEach(function(part, index){
                    if(index != name.length - 1){
                        children.push(React.createElement("span", { key: part + index }, part));
                        children.push(React.createElement("br", { key: part + "br" + index }, null));
                    }
                    else{
                        children.push(React.createElement("span", { key: part + index }, part));
                    }
                });

                heading = <div className="slider_nav__text">{ children }</div>;
            }
            else{
                heading = <div className="slider_nav__text">{ this.props.name }</div>;
            }
        }

        return (
            <button data-key={ this.props.key } ref="category" type="button" className={ categoryClass }>
                <i className={ iconClass }></i>
                { heading }
            </button>
        )
    }
});