/** @jsx React.DOM */

var React = require('react'),
    CategoryNavigation = require("./CategoryNavigation"),
    CategoryDisplay = require("./CategoryDisplay")
    ;

module.exports = KioskApp = React.createClass({
    propTypes: {
        categories: React.PropTypes.array.isRequired,
        contacts: React.PropTypes.array.isRequired
    },
    render: function() {

        var categories = this.props.categories;
        var contacts = this.props.contacts;

        return (
            <div className="wrapper" id="wrapper">
                <CategoryDisplay initialCategory={ categories[0] } contacts={ contacts }/>
                <CategoryNavigation categories={ categories } autoScrollIntervalInMs={25000} enableAutoScroll={true}/>
            </div>
        )
    }
});