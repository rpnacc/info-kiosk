/** @jsx React.DOM */

var React = require('react'),
    ReactDOM = require('react-dom'),
    Contacts = require('./Contacts'),
    CategoryTitle = require('./CategoryTitle'),
    CategoryButtonsBlock = require('./CategoryButtons'),
    CategoryNotesList = require('./CategoryNotesList');

module.exports = CategoryDisplay = React.createClass({
    propTypes: {
        initialCategory: React.PropTypes.object.isRequired,
        contacts: React.PropTypes.array.isRequired
    },

    getInitialState(){
        return {
            currentCategory: null,
            wasRendered: false
        }
    },

    componentDidMount: function() {
        window.addEventListener("select-category:event", this.handleCategorySelectEvent, false);
        window.addEventListener("play-category-animation:event", this.playCategorySelectAnimation, false);
        window.addEventListener("animationend", this.handleAnimation, false);
        var node =  ReactDOM.findDOMNode(this.refs.categoryDisplay);
        node.addEventListener("webkitTransitionEnd", this.handleTransition, false);
    },

    componentWillUnmount: function() {
        window.removeEventListener("select-category:event", this.handleCategorySelectEvent, false);
        window.removeEventListener("play-category-animation:event", this.playCategorySelectAnimation, false);
        window.removeEventListener("animationend", this.handleAnimation, false);
        var node =  ReactDOM.findDOMNode(this.refs.categoryDisplay);
        node.removeEventListener("webkitTransitionEnd", this.handleTransition, false);

    },
    handleAnimation: function(event){
        var node = event.target;
        if(node){
            node.classList.remove('animated');
        }
    },

    handleTransition: function(event){
        var node = event.currentTarget;
        if(node){
            node.classList.remove('slider_main__item_fade');
        }
    },

    playCategorySelectAnimation: function(){
        if(!this.isMounted()) {
            return;
        }

        var node =  ReactDOM.findDOMNode(this.refs.categoryDisplay);

        var elements = node.querySelectorAll('.wow');

        Array.prototype.forEach.call(elements, function(elem){
            elem.classList.add('anim-out');
        });

        if(this.state.wasRendered){
            node.classList.add('slider_main__item_fade');
        }
        else{
            this.state.wasRendered = !this.state.wasRendered;
        }
    },

    handleCategorySelectEvent: function(event){
        var self = this;

        if(!this.isMounted()) {
            return;
        }
        setTimeout(function() {
            self.setState({
                currentCategory: event.detail.category
            });
        }, 500);
    },

    render: function() {

        if(!this.state.wasRendered && this.props.initialCategory){
            this.state.currentCategory = this.props.initialCategory;
        }

        var category = this.state.currentCategory;
        var contacts = this.props.contacts;
        var content = this.state.currentCategory.Content;

        var categoryIcon;
        if(!content.hasOwnProperty("DoNotShowBigIcon")){
            var displayIconClass = "ico i-icon-big wow fadeInUp animated " + content.IconCss + "_big";
            categoryIcon = <div key={content.Key + "Icon"} className={ displayIconClass }></div>;
        }

        return (
            <div className="slider slider_main" id="slider-main">
                <div className="slider__item slider_main__item slider_main__item_active" ref="categoryDisplay">

                        <CategoryTitle key={content.Key + "Title"} title={ content.Name }/>

                        <div key={content.Key + "Coat"} className="ico ico-coat i-coat wow fadeIn animated"></div>

                        { categoryIcon }

                        <div key={content.Key + "WantWrap"} className="ico ico-want i-line i-line_want wow slideInRight animated">
                            <div className="i-line__wrap">
                                <div key={content.Key + "WantText"} className="i-line__text wow slideInDown animated">{ content.Want }</div>
                            </div>
                        </div>

                        <div key={content.Key + "CanWrap"} className="ico ico-can i-line i-line_can wow slideInLeft animated">
                            <div className="i-line__wrap">
                                <div key={content.Key + "CanText"} className="i-line__text i-line_can__text wow slideInDown animated">{ content.Can }</div>
                            </div>
                        </div>

                        <div key={content.Key + "KnowWrap"} className="ico ico-know i-line i-line_know wow slideInRight animated">
                            <div className="i-line__wrap"></div>
                        </div>

                        <div className="i-line__text i-line_know__text">{ content.Know }</div>

                        <Contacts key={content.Key + "Contacts"} contacts={ contacts }/>

                        <CategoryNotesList notes={ content.List } categoryName={ content.Key }/>

                        <CategoryButtonsBlock category={ category }/>
                </div>
            </div>
        )
    }
});