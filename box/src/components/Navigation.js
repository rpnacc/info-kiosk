/** @jsx React.DOM */

var React = require('react'),
    ReactDOM = require('react-dom');

module.exports = Navigation = React.createClass({
    componentDidMount: function(){
        var node = ReactDOM.findDOMNode(this);

        var swiper = new Swiper(node, {
            direction: 'horizontal',
            autoplay: false,
            autoplayDisableOnInteraction: false,
            //loop: true,
            loopAdditionalSlides: 0,
            spaceBetween: 0,
            slidesPerView: 'auto',
            //prevButton: navBtnPrev,
            //nextButton: navBtnNext,
            centeredSlides: true,
            slideToClickedSlide: true,
            onInit: function(obj){
                var duplicates = document.querySelectorAll('.swiper-slide-duplicate');
                Array.prototype.forEach.call(duplicates, function(duplicate){
                    var id = duplicate.getAttribute("data-reactid");
                    if(id){
                        duplicate.setAttribute("data-reactid", id + "dublicate");
                    }
                });

            }
        });

        var component = ( <div className="slider slider_nav" id="slider-nav" ref="navigation">
            <div className="slider_nav__wrap swiper-container" id="slider-nav-wrap" ref="navWrap">
                { this.props.children }
            </div>
            <div className="slider_nav__arrows">
                <button type="button" ref="navBtnPrev" className="btn slider_nav__btn slider_nav__btn_left swiper-button-prev" onClick={ this.prevCategory }/>
                <button type="button" ref="navBtnNext" className="btn slider_nav__btn slider_nav__btn_right swiper-button-next" onClick={ this.nextCategory }/>
            </div>
        </div>);

        React.render(component, node);
    },

    render: function() {
       return (
           <div />
       )
    }
});