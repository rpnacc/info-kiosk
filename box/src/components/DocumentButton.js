/** @jsx React.DOM */

var React = require('react'),
    config = require('../config'),
    ReactDOM = require('react-dom'),
    ModalOpenCloseMixin = require('./Mixes/ModalOpenCloseMixin'),
    Modal = require('./Modal');

var title = "ЧИТАТЬ";
var localUrl = config.host + ":" + config.port;
var PDF_VIEWER = localUrl + "/js/pdfjs-dist/web/viewer.html?file=" + localUrl;
var PDF_VIWERE_PAGE = "#page=";

module.exports = DocumentButton = React.createClass({
    propTypes: {
        documents: React.PropTypes.array.isRequired,
        idleLimitInMs: React.PropTypes.number,
        isTrackingEnabled: React.PropTypes.bool
    },

    mixins: [ModalOpenCloseMixin],

    getInitialState: function() {
        return {
            modalIsOpen: false,
            currentDocumentIndex: 0,
            page: 1
        };
    },

    getDefaultProps: function(){
        return {
            documents: [],
            isTrackingEnabled: true,
            idleLimitInMs: 60000
        }
    },

    render: function() {
        var self = this;
        var currentDocument = this.props.documents[this.state.currentDocumentIndex];

        var pdfViewer;
        if(currentDocument) {
            var file = PDF_VIEWER + currentDocument + PDF_VIWERE_PAGE + this.state.page;
            pdfViewer =  (<iframe src= { file } className="frame-viewer" id="pdf-viewer"></iframe>)
        }

        return (
            <div className="i-btns__item wow fadeIn animated">
                <button className="btn i-btns__btn" onClick={ this.openModal }>
                    <i className="ico ico-arrow i-btns__icon"/>

                    <span className="i-btns__text">{ title }</span>
                </button>

                <Modal isModalOpen={ self.state.modalIsOpen }
                       contentId = { "pdf-viewer" }
                       isTrackingEnabled = { this.props.isTrackingEnabled }
                       closeModal =  { self.closeModal }
                       activityTimeoutInMs = { this.props.idleLimitInMs }>
                    { pdfViewer }
                </Modal>
            </div>
        )
    }
});