/** @jsx React.DOM */

var React = require('react');

module.exports = CategoryTitle = React.createClass({
    propTypes: {
        title: React.PropTypes.any.isRequired,
    },
    render: function() {

        var title = this.props.title;

        var heading;
        if(title instanceof Array){
            var children = [];
            title.forEach(function(part, index){
                if(index != name.length - 1){
                    children.push(React.createElement("span", { key: part + index }, part));
                    children.push(React.createElement("br", { key: part + "br" + index }, null));
                }
                else{
                    children.push(React.createElement("span", { key: part + index }, part));
                }
            });

            heading = <div className="ico ico-circle i-circle wow fadeIn animated"><div className="i-circle__text">{ children }</div></div>;
        }
        else{
            heading = <div className="ico ico-circle i-circle wow fadeIn animated"><div className="i-circle__text">{ this.props.title }</div></div>;
        }


        return (heading);
    }
});