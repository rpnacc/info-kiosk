/** @jsx React.DOM */

var React = require('react');

module.exports = CategoryNotesList = React.createClass({
    propTypes: {
        categoryName: React.PropTypes.string.isRequired,
        notes: React.PropTypes.array.isRequired,
    },

    render: function() {

        var name = this.props.categoryName;
        var index=0;

        var notes = this.props.notes.map(function(note) {
            ++index;
            var key = name + index;

            return (
                <li key={ key } className="i-list__item wow fadeInDown animated">{ note }</li>
            )
        });

        return (
            <ul className="i-list">
                { notes }
            </ul>
        )
    }
});