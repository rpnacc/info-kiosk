/** @jsx React.DOM */

var React = require('react'),
    Category = require('./Category'),
    ReactDOM = require('react-dom'),
    ReactDOMServer = require('react-dom/server');

module.exports = CategoryNavigation = React.createClass({
    propTypes: {
        categories: React.PropTypes.array.isRequired,
        stepInMs: React.PropTypes.number,
        autoScrollIntervalInMs: React.PropTypes.number,
        enableAutoScroll: React.PropTypes.bool
    },

    getDefaultProps: function(){
        return {
            categories: [],
            stepInMs: 1000,
            autoScrollIntervalInMs: 10000,
            enableAutoScroll: false
        }
    },

    getInitialState: function(){
        return {
            selectedCategory: this.props.categories[0].Content.Key,
            animationInProgress: false,
            idleTimeInMs: 0
        }
    },

    componentDidMount(){
        var self = this;

        this.swiper = this.setupSwiper();

        this.swiper.on('click', function(){
            self.removeIdle();
        });

        this.swiper.on('onSlideChangeStart', function(){
            self.setState({ animationInProgress: true });
            self.triggerCategoryAnimation();
        });
        
        this.swiper.on('onSlideChangeEnd', function(swiper) {
            var activeSlide = document.querySelector('.swiper-slide-active');
            var categoryIndex = Number(activeSlide.getAttribute("data-swiper-slide-index"));
            var categoryKey = self.props.categories[categoryIndex].Content.Key;

            swiper.fixLoop();

            self.switchCategory(categoryKey);
            self.setState({ animationInProgress: false });
        });

        this.selectCategory(this.state.selectedCategory);

        if(this.props.enableAutoScroll){
            this.interval = setInterval(this.autoScroll, this.props.stepInMs);
        }
    },

    componentWillUnmount: function() {
        if(this.props.enableAutoScroll){
            clearInterval(this.interval);
        }

        this.swiper.off('click');
        this.swiper.off('onSlideChangeEnd');
    },

    shouldComponentUpdate: function(nextProps, nextState) {
       return nextProps.categories.length !== this.props.categories.length;
    },


    getCategories: function(){
        var selectedCategory = this.state.selectedCategory;
        var isSelected = false;

        var rawCategories = this.props.categories.sort(function(cat1, cat2){
            if (cat1.Content.Order > cat2.Content.Order) {
                return 1;
            }
            if (cat1.Content.Order < cat2.Content.Order) {
                return -1;
            }
            return 0;
        });

        var categories = rawCategories.map(function(category) {

            if(selectedCategory == null){
                selectedCategory =  category.Content.Key;
            }

            isSelected = category.Content.Key === selectedCategory;

            var showCategoryTitle = category.Content.DoNotShowCategoryTitle != undefined ?
                !category.Content.DoNotShowCategoryTitle : true;

            return <Category
                key={ category.Content.Key }
                name={ category.Content.Name }
                iconClass={ category.Content.IconCss }
                showCategoryTitle={ showCategoryTitle }
                isSelected={ isSelected }
            />
        });

        return categories;
    },

    setupSwiper: function(){
        var navListWrap = ReactDOM.findDOMNode(this.refs.navWrap);
        var navBtnPrev = ReactDOM.findDOMNode(this.refs.navBtnPrev);
        var navBtnNext = ReactDOM.findDOMNode(this.refs.navBtnNext);

        var categories = this.getCategories();
        var element = (
            <div className="slider_nav__frame swiper-wrapper" id="slider-nav-frame">
                { categories }
            </div>
        );

        var html = ReactDOMServer.renderToStaticMarkup(element);
        navListWrap.innerHTML = html;

        var swiper = new Swiper(navListWrap,
            {
                direction: 'horizontal',
                autoplayDisableOnInteraction: false,
                loop: true,
                spaceBetween: 0,
                slidesPerView: 'auto',
                prevButton: navBtnPrev,
                nextButton: navBtnNext,
                centeredSlides: true,
                slideToClickedSlide: true,
            }
        );

        return swiper;
    },

    autoScroll: function(){

        var idleTime = this.state.idleTimeInMs + this.props.stepInMs;
        this.setState({ idleTimeInMs: idleTime });

        if(window.isOpenModal) {
            return;
        }

        if(this.state.animationInProgress){
            return;
        }

        if(idleTime >= this.props.autoScrollIntervalInMs){
            if(!window.isOpenModal){
                this.nextCategory();
            }
        }
    },

    triggerCategoryAnimation: function(){
        var playCategoryAnimationEvent = new CustomEvent("play-category-animation:event", { bubbles: true });

        var node = ReactDOM.findDOMNode(this.refs.navigation);

        if(node){
            node.dispatchEvent(playCategoryAnimationEvent);
        }
    },

    selectCategory: function(categoryKey){
        var category = this.props.categories.find(function(category, index){
            return category.Content.Key === categoryKey;
        });

        this.setState({ selectedCategory : categoryKey });

        var selectCategoryEvent = new CustomEvent("select-category:event", {
            detail: {
                category: category
            },
            bubbles: true
        });

        var node = ReactDOM.findDOMNode(this.refs.navigation);

        if(node){
            node.dispatchEvent(selectCategoryEvent);
        }
    },

    switchCategory: function(categoryKey) {
        this.selectCategory(categoryKey);
        this.removeIdle();
    },

    nextCategory: function(){
        this.swiper.slideNext();
        this.removeIdle();
    },

    prevCategory: function() {
        this.swiper.slidePrev();
        this.removeIdle();
    },

    removeIdle: function(){
        this.setState({idleTimeInMs: 0});
    },

    render: function() {

        return (
            <div className="slider slider_nav" id="slider-nav" ref="navigation">
                <div className="slider_nav__wrap swiper-container" id="slider-nav-wrap" ref="navWrap">
                </div>
                <div className="slider_nav__arrows">
                    <button type="button" ref="navBtnPrev" className="btn slider_nav__btn slider_nav__btn_left swiper-button-prev" onClick={ this.prevCategory }/>
                    <button type="button" ref="navBtnNext" className="btn slider_nav__btn slider_nav__btn_right swiper-button-next" onClick={ this.nextCategory }/>
                </div>
            </div>
        )
    }
});
