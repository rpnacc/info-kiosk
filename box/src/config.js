var env = require('../env.json');

function getConfig() {
    var node_env = process.env.NODE_ENV || 'development';
    var  config = env[node_env];
    config["root"] = __dirname;
    config["host"] = process.env.WEBSITE_HOSTNAME || 'http://localhost';
    config["port"] = process.env.PORT | 40080;
    return config;
};

var config = getConfig();

module.exports = config;