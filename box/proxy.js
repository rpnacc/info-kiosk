var fs = require('fs'),
    path = require('path'),
    foreverMonitor = require('./tools/forever-monitor')('src/server.js'),
    setSystemMonitor = require('./tools/set-system-monitor'),
    startChrome = require('./tools/start-chrome'),
    logger = require('./tools/logger'),
    gitPull = require('./tools/git-pull'),
    config = require('./src/config'),
    CronJob = require('cron').CronJob;

var cronJob = null;
var isUpdateInProgress = false;

var closeProxy = function(){
    var exitProcess = function(){
        process.exit(0);
    };
    logger.once(exitProcess); // attempting to send all long including last ones

    setTimeout(function(){
        exitProcess();
    }, 5000); // shut down in 5 s anyway
};

var proxy = {
    clean: function(err){
        if(err){
            logger.error("cleaning up after error, " + err);
        }
        else{
            logger.info("cleaning up after the proxy");
        }

        try {
            foreverMonitor.stopServer();
            if(cronJob){
                cronJob.stop();
            }
            setSystemMonitor();
        }
        catch (err){
            logger.error("error on cleaning, " + err);
        }
        finally {
            logger.info("proxy setting up a restart");
            closeProxy();
        }
    },

    start: function(){
        var startInBrowser = function(){
            foreverMonitor.startServer(config.uid, function onServerStarted(){
                setTimeout(function(){
                    startChrome(config.host, config.port, function(err){
                        if(err){
                            logger.error("chrome error, " + err);
                        }
                    });
                }, 2000);
            });
        };

        foreverMonitor.list(config.uid, function(err, procs){
            if(err){
                logger.error("forever list error, " + err);
            }

            if(procs && procs.length > 0) {
                logger.info("Instance of " + config.uid + " is already up and running.");
            }
            else{
                return startInBrowser();
            }
        });
    },

    checkForUpdates: function(firstRun, retryCnt){
        var self = this,
            retryCnt = retryCnt || 0,
            firstRun = firstRun || false;

        isUpdateInProgress = true;

        var handleGitPullResults = function(err, res, isUpToDate, hasLocalChanges){
            if(err){
                logger.error("git pull error, " + res);

                if(!firstRun && retryCnt > 0){
                    logger.info("attempt to retry an update");
                    return self.checkForUpdates(firstRun, --retryCnt);
                }
            }
            else{
                if(hasLocalChanges){
                    logger.info("proxy detected local changes at application directory");
                    return self.clean();
                }

                if(!isUpToDate){
                    logger.info("current application version is out of date");
                    return self.clean();
                }
            }

            self.start();

            isUpdateInProgress = false;
        };

        return gitPull(handleGitPullResults);
    },

    run: function(){

        var self = this;

        logger.info("setting up cron job");

        cronJob = new CronJob(config.cronPattern,
            function work(){
                logger.info("on cron job exec");

                if(!isUpdateInProgress){
                    self.checkForUpdates(false, config.retryCnt || 5);
                }
            },
            function onComplete() {
                logger.info("cron stopped");

            },
            true /* Start the job right now */
        );

        logger.info("starting the proxy, selected configuration - " + process.env.NODE_ENV);

        // initial start, we should either start a kiosk or go to an update circle
        self.checkForUpdates(true);
    }
};


process.on('SIGINT', proxy.clean.bind(proxy));
process.on('uncaughtException', proxy.clean.bind(proxy));


proxy.run();

