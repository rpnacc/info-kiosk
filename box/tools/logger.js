
var Logger = require('le_node'),
    config = require('../src/config'),
    clientId = require('./client-id');

var leLogger, isLoggerAvailable = false;

if(config.leToken){
    leLogger = new Logger({
        token:config.leToken,
        console: true
    });
    leLogger.on("error", function(err){
        console.log("le_node error " + err);
        isLoggerAvailable = false;
    });

    leLogger.on("connected", function(){
        isLoggerAvailable = true;
    });

    isLoggerAvailable = true;
}

var addDate= function(msg){
    var currentDate = '[' + new Date().toUTCString() + '] ';
    return currentDate + msg;
};

var addClientId = function(msg){
    return clientId + ' ' + msg;
};

module.exports = {
    info: function(msg){
        if(isLoggerAvailable){
            leLogger.info(addClientId(msg));
        }
        else{
            console.info(addDate(addClientId(msg)));
        }
    },
    error: function(msg){
        if(isLoggerAvailable){
            leLogger.log("err", addClientId(msg));
        }
        else{
            console.error(addDate(addClientId(msg)));
        }
    },
    once: function(cb){
        if(isLoggerAvailable){
            leLogger.once("connection drain", function(){
                cb();
            })
        }
        else{
            return cb();
        }
    }
};