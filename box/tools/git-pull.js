exec = require('child_process').exec;

var ALREADY_UP_TO_DATE = "Already up-to-date";
var LOCAL_CHANGES = "Please, commit your changes or stash them before you can merge.";

module.exports = function(callback){
    var isUpToDate = true,
        hasLocalChanges = false;
    exec('git pull origin master', [], function(error, stdout, stderr) {
        if(error){
            if(error.message.indexOf(LOCAL_CHANGES) > -1){
                hasLocalChanges = true;
                return callback(null, stderr.trim(), isUpToDate, hasLocalChanges); // force restart
            }
            return callback(error, stderr.trim(), isUpToDate);
        }
        var res = stdout.trim();
        isUpToDate = res.indexOf(ALREADY_UP_TO_DATE) > -1;
        return callback(null, res, isUpToDate);
    });
};
