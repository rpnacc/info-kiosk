var fs = require('fs'),
    path = require('path'),
    config = require('../src/config');

var STARTUP_MONITOR_PATH = path.join(config.root, config.systemMonitoringFile);

module.exports = function(){
    fs.writeFileSync(STARTUP_MONITOR_PATH, "OK");
};