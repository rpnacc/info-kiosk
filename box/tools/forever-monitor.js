var Forever = require('forever');

module.exports = function(srvScript){
    var SERVER_SCRIPT = srvScript;
    return {
        stopServer: function(){
            Forever.stop(SERVER_SCRIPT);
        },
        startServer: function(uid, callback) {
            var server = Forever.start(SERVER_SCRIPT, {
                "uid": uid,
                "append": true,
                "watch": true,
                'killTree': true,
                "script": SERVER_SCRIPT,
                "watchDirectory": "src/",
                'watchIgnoreDotFiles': null, // Whether to ignore file starting with a '.'
                'watchIgnorePatterns': null // Ignore patterns to use when watching files.
            });
            Forever.startServer(server, callback);
        },
        list: function(uid, callback){
            Forever.list(undefined, function(err, processes){
                if(err){
                    return callback(err);
                }

                return callback(null, Forever.findByUid(uid, processes));
            });
        }
    };
};