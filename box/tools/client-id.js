var fs = require('fs'),
    path = require('path'),
    uuid = require('node-uuid'),
    config = require('../src/config');

var CLIENT_ID = path.join(config.root, '../client.id');

var clientId;
if(!fs.existsSync(CLIENT_ID)) {
    clientId = uuid.v1();
    fs.writeFileSync(CLIENT_ID, clientId);
}
else{
    clientId = fs.readFileSync(CLIENT_ID);
}

module.exports = clientId;