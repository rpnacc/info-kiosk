
exec = require('child_process').exec;

var commandHandler = function(err, stdout, stderr){
    console.log("stdout: " + stdout);
    console.log("stderr: " + stderr);

    if(err) {
        console.log("error occured - " + err);
    }
};

module.exports = function(host, port, customHandler){
    var command = "chrome -kiosk --incognito --disable-pinch --overscroll-history-navigation=0 " + host + ":" + port;
    var handler =   customHandler || commandHandler;
    exec(command, handler);
};